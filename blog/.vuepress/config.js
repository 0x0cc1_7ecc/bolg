// .vuepress/config.js
module.exports = {
    title: 'La Forge', // Title for the site. This will be displayed in the navbar.
    theme: '@vuepress/theme-blog',
    themeConfig: {
      // Please keep looking down to see the available options.
      feed: {
        canonical_base:'https://lafor.ge',
        rss:true,
        atom:true,
        json:false
      },
      sitemap: {
        hostname: "https://lafor.ge"
      },
      smoothScroll: true,
      footer: {
        contact: [
          {
            type: 'gitlab',
            link: 'https://gitlab.com/akanoa',
          },
          {
            type: 'twitter',
            link: 'https://twitter.com/_Akanoa_',
          },
        ],
      },
    },
    dest: "public",
    base: "/",
    plugins: [
      ['container', {
        type: 'tip',
        defaultTitle: {
          '/': 'TIP',
          '/zh/': '提示'
        }
      }],
      ['container', {
        type: 'warning',
        defaultTitle: {
          '/': 'WARNING',
          '/zh/': '注意'
        }
      }],
      ['container', {
        type: 'danger',
        defaultTitle: {
          '/': 'DANGER',
          '/zh/': '警告'
        }
      }],
      ['container', {
        type: 'details',
        defaultTitle: "Cliquez pour dérouler",
        before: info => `<details class="custom-block details">${info ? `<summary>${info}</summary>` : ''}\n`,
        after: () => '</details>\n'
      }],
      ['container', {
        type: 'info',
        defaultTitle: "Information",
      }],
      // you can use this plugin multiple times
      'vuepress-plugin-mermaidjs',
      [
        '@akanoa/vuepress-plugin-goatcounter', {
          user: 'laforge'
        }
      ],
      [
          '@vssue/vuepress-plugin-vssue', {
              platform: 'gitlab',
              owner: 'Akanoa',
              repo: 'blog-comment',
              locale: 'fr',

              clientId: `${process.env.GITLAB_CLIENT_ID}`
          }
      ],
      ['vuepress-plugin-code-copy', {
        selector: 'div[class*="language-"]'
      }]
    ],
  }